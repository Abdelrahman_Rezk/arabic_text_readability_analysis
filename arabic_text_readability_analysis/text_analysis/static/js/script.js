$(document).ready(function(){

	$('.home_page .user_input_form .submit_text').click(function(e){
		e.preventDefault(); // prevent default action of form submitted

		// The div we append text to if there are a text then remove it
		$(".home_page .user_input_form .user_input_result").empty();

		// retrieve user text input
		var user_text = $('.home_page .user_input_form textarea').val(),
		csrfmiddlewaretoken = $(".home_page .user_input_form").find('input[name="csrfmiddlewaretoken"]').val();
		
        // Send Ajax request for get and update user data
        $.ajax({
            url: '', // ipdate at the same url
            method: 'POST',
            headers: {
                // Secure key for forms its common for django
                'X-CSRFToken': csrfmiddlewaretoken,
            },
            data: JSON.stringify({'user_text': user_text}),
            success: function (data) {
                $(".home_page .num_paragraph").append("<span>" + data['num_paragraph'] + "</span>");
                $(".home_page .num_of_sentence").append("<span>" + data['num_of_sentence'] + "</span>");
                $(".home_page .word_numbers").append("<span>" + data['word_numbers'] + "</span>");
                $(".home_page .num_of_syllable").append("<span>" +data['num_of_syllable'] + "</span>");
            }
        })
	})

}); 

