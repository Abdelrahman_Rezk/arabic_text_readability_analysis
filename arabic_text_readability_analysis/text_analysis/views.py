from django.shortcuts import render
from django.http import JsonResponse,HttpResponse
import json

# file that contain our statics functions
from .statics_gunning_fog_index import * 
# Create your views here.

def home_page(request):
	data = {}
	# check user request
	if request.method == 'POST':

		data = json.loads(request.body) # get data hat user inout in text area
		#get required statics of the user input text
		num_of_sentence, num_paragraph = get_number_of_sentence_paragraph(data['user_text'])
		num_paragraph -=1
		word_numbers, uniques_words_numbers, chars_number = get_number_of_words_char_unique(data['user_text'])
		num_of_syllable = get_num_syllables(data['user_text'])
		
		# update the json before return to jquery
		data['num_paragraph'] = num_paragraph
		data['num_of_sentence'] = num_of_sentence
		data['word_numbers'] = word_numbers
		data['num_of_syllable'] = num_of_syllable
		return JsonResponse(data)
	
	return render(request, 'text_analysis/home_page.html',)
	

